import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MovieToolBarComponent } from './movie-tool-bar/movie-tool-bar.component';
import { MovieListComponent } from './movie-list/movie-list.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule, MatFormFieldModule, MatDialogModule, MatInputModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { CdkDetailRowDirective } from './movie-list/cdk-detail-row.directive';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MovieApiService } from './shared/movie-api.service';
import { MovieRefreshService } from './shared/movie-refresh.service';
import { MessageService } from './shared/message.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MovieToolBarComponent,
        MovieListComponent,
        CdkDetailRowDirective
      ],
      imports: [        
    FormsModule,
	  ReactiveFormsModule,    
    MatDividerModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatExpansionModule,
    MatListModule,
    MatTableModule,
    MatSelectModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatIconModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule
      ],
    providers: [MovieApiService, MovieRefreshService, MessageService],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title ':) Movie Searcher'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual(':) Movie Searcher');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain(':) Movie Searcher');
  }));
});
