import { TestBed, inject } from '@angular/core/testing';

import { MovieApiService } from './movie-api.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule, MatFormFieldModule, MatDialogModule, MatInputModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { CdkDetailRowDirective } from '../movie-list/cdk-detail-row.directive';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MovieRefreshService } from '../shared/movie-refresh.service';
import { MessageService } from '../shared/message.service';
import { Movie } from '../app.component';

import { asyncData, asyncError } from './async-observable-helpers';

describe('DataService', () => {   

  let httpClientSpy: { get: jasmine.Spy };
  let service: MovieApiService;

  beforeEach(() => {

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new MovieApiService(<any> httpClientSpy, new MessageService());

    TestBed.configureTestingModule({
        providers: [MovieApiService, MovieRefreshService, MessageService],
        imports: [        
          FormsModule,
          ReactiveFormsModule,    
          MatDividerModule,
          MatFormFieldModule,
          MatDialogModule,
          MatInputModule,
          MatExpansionModule,
          MatListModule,
          MatTableModule,
          MatSelectModule,
          MatButtonModule,
          MatSlideToggleModule,
          MatCheckboxModule,
          MatIconModule,
          HttpModule,
          HttpClientModule,
          BrowserAnimationsModule
            ],    
    });
  });

  it('should be created', inject([MovieApiService], (service: MovieApiService) => {
    expect(service).toBeTruthy();
  }));

  it('can call getMovies', () => {

    const expectedMovies: Movie[] = [{ movieId: 1, title: 'A', classification: '', 
                                       genre: 'Thriller', rating: 1, releaseDate: 1999, 
                                       cast:['Tom Hanks'], castStr:'' }];

   httpClientSpy.get.and.returnValue(asyncData(expectedMovies));

   service.getMovies("").subscribe(
    heroes => expect(heroes).toEqual(expectedMovies, 'expected movies'),
    fail
  );
  expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');

  });

});
