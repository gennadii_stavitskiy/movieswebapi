import { TestBed, inject } from '@angular/core/testing';

import { MovieApiService } from './movie-api.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule, MatFormFieldModule, MatDialogModule, MatInputModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { CdkDetailRowDirective } from '../movie-list/cdk-detail-row.directive';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MovieRefreshService } from '../shared/movie-refresh.service';
import { MessageService } from '../shared/message.service';
import { Movie } from '../app.component';

import { asyncData, asyncError } from './async-observable-helpers';

describe('MessageService', () => {   
  
  let service: MessageService;

  beforeEach(() => {
    
    service = new MessageService();

    TestBed.configureTestingModule({
        providers: [MovieApiService, MovieRefreshService, MessageService],
        imports: [        
          FormsModule,
          ReactiveFormsModule,    
          MatDividerModule,
          MatFormFieldModule,
          MatDialogModule,
          MatInputModule,
          MatExpansionModule,
          MatListModule,
          MatTableModule,
          MatSelectModule,
          MatButtonModule,
          MatSlideToggleModule,
          MatCheckboxModule,
          MatIconModule,
          HttpModule,
          HttpClientModule,
          BrowserAnimationsModule
            ],    
    });
  });

  it('should be created', inject([MessageService], (service: MessageService) => {
    expect(service).toBeTruthy();
  }));

  it('can add message', () => {      
    service.add("New message.")

    expect(service.messages.length).toBe(1, 'one message');
  });

  it('can clean messages', () => {
    service.add("New message 1");
    service.add("New message 2");
    
    expect(service.messages.length).toBe(2, '2 messages');

    service.clear();
    
    expect(service.messages.length).toBe(0, 'Empty');
  });

});
