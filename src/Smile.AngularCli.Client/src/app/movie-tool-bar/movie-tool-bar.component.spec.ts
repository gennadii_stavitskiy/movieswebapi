import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieToolBarComponent } from './movie-tool-bar.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule, MatFormFieldModule, MatDialogModule, MatInputModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { CdkDetailRowDirective } from '../movie-list/cdk-detail-row.directive';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MovieApiService } from '../shared/movie-api.service';
import { MovieRefreshService } from '../shared/movie-refresh.service';
import { MessageService } from '../shared/message.service';

describe('MovieToolBarComponent', () => {
  let component: MovieToolBarComponent;
  let fixture: ComponentFixture<MovieToolBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieToolBarComponent ],
      imports: [        
        FormsModule,
        ReactiveFormsModule,    
        MatDividerModule,
        MatFormFieldModule,
        MatDialogModule,
        MatInputModule,
        MatExpansionModule,
        MatListModule,
        MatTableModule,
        MatSelectModule,
        MatButtonModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatIconModule,
        HttpModule,
        HttpClientModule,
        BrowserAnimationsModule
          ],       
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieToolBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
