﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smile.Movies.Web.Model
{
    public class ClientAppSettings
    {
        public string ApiEndpoint
        {
            get;
            set;
        }
    }
}
