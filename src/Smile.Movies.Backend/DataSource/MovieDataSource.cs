﻿using Smile.Movies.Backend.DataSource;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Threading;

namespace MoviesLibrary
{
    public class MovieDataSource
    {
        private static DataSet _dsMovies = new DataSet();
        private static int _pk;

        private static DataSet Movies
        {
            get
            {
                lock (MovieDataSource._dsMovies)
                {
                    if (MovieDataSource._dsMovies.Tables.Count > 0)
                    {
                        return MovieDataSource._dsMovies;
                    }
                        
                    //StreamReader streamReader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Smile.Movies.Backend.DataSource.MoviesDataSource.xml"));
                    StringReader theReader = new StringReader(Resources.MoviesDataSource);
                    MovieDataSource._dsMovies = new DataSet();

                    int num = (int)MovieDataSource._dsMovies.ReadXml(theReader, XmlReadMode.ReadSchema);

                    if (MovieDataSource._dsMovies.Tables["Movie"].PrimaryKey == null || MovieDataSource._dsMovies.Tables["Movie"].PrimaryKey.Length == 0)
                    {
                        MovieDataSource._dsMovies.Tables["Movie"].PrimaryKey = new DataColumn[1]
                        {
                            MovieDataSource._dsMovies.Tables["Movie"].Columns["Id"]
                        };
                    }

                    DataView dataView = new DataView(MovieDataSource._dsMovies.Tables["Movie"], (string)null, "Id DESC", DataViewRowState.CurrentRows);
                    MovieDataSource._pk = dataView.Count <= 0 ? 0 : Convert.ToInt32(dataView[0]["Id"]);

                    return MovieDataSource._dsMovies;
                }
            }
        }

        public List<MovieData> GetAllData()
        {
            List<MovieData> movieDataList = new List<MovieData>();
            if (MovieDataSource.Movies != null)
            {
                foreach (DataRow row in (InternalDataCollectionBase)MovieDataSource.Movies.Tables["Movie"].Rows)
                    movieDataList.Add(this.GetMovieData(row));
                Thread.Sleep(2000);
            }
            return movieDataList;
        }

        public MovieData GetDataById(int id)
        {
            if (MovieDataSource.Movies != null)
            {
                Thread.Sleep(100);
                DataRow[] dataRowArray = MovieDataSource.Movies.Tables["Movie"].Select("Id = " + id.ToString());
                if (dataRowArray.Length > 0)
                    return this.GetMovieData(dataRowArray[0]);
            }
            return (MovieData)null;
        }

        public int Create(MovieData movie)
        {
            if (MovieDataSource.Movies == null)
                throw new Exception("Movies datasource is not available");
            lock (MovieDataSource._dsMovies)
            {
                DataRow row = MovieDataSource._dsMovies.Tables["Movie"].NewRow();
                int num = ++MovieDataSource._pk;
                movie.MovieId = num;
                row["Id"] = (object)num;
                if (string.IsNullOrEmpty(movie.Title))
                    throw new Exception("Movie Title is mandatory");
                row["Title"] = (object)movie.Title.Trim();
                if (!string.IsNullOrEmpty(movie.Genre))
                    row["Genre"] = (object)movie.Genre.Trim();
                if (!string.IsNullOrEmpty(movie.Classification))
                    row["Classification"] = (object)movie.Classification.ToString();
                row["Rating"] = (object)movie.Rating;
                row["ReleaseDate"] = (object)movie.ReleaseDate;
                MovieDataSource._dsMovies.Tables["Movie"].Rows.Add(row);
                if (movie.Cast != null && movie.Cast.Length > 0)
                    this.AddCast(movie);
                MovieDataSource._dsMovies.AcceptChanges();
                return num;
            }
        }

        public void Update(MovieData movie)
        {
            if (MovieDataSource.Movies == null)
                throw new Exception("Movies datasource is not available");
            lock (MovieDataSource._dsMovies)
            {
                DataRow[] dataRowArray = MovieDataSource._dsMovies.Tables["Movie"].Select("Id = " + (object)movie.MovieId);
                if (dataRowArray.Length == 0)
                    throw new Exception("The movie ID " + movie.MovieId.ToString() + " does not exist");
                if (string.IsNullOrEmpty(movie.Title))
                    throw new Exception("Movie Title is mandatory");
                dataRowArray[0]["Title"] = (object)movie.Title;
                dataRowArray[0]["Classification"] = string.IsNullOrEmpty(movie.Classification) ? (object)DBNull.Value : (object)movie.Classification.Trim();
                dataRowArray[0]["Genre"] = string.IsNullOrEmpty(movie.Genre) ? (object)DBNull.Value : (object)movie.Genre.Trim();
                dataRowArray[0]["Rating"] = (object)movie.Rating;
                dataRowArray[0]["ReleaseDate"] = (object)movie.ReleaseDate;
                foreach (DataRow dataRow in MovieDataSource._dsMovies.Tables["Cast"].Select("MovieId = " + movie.MovieId.ToString()))
                    dataRow.Delete();
                if (movie.Cast == null || movie.Cast.Length <= 0)
                    return;
                this.AddCast(movie);
            }
        }

        private void AddCast(MovieData movie)
        {
            for (int index = 0; index < movie.Cast.Length; ++index)
            {
                if (movie.Cast[index].Trim().Length > 0)
                {
                    DataRow row = MovieDataSource._dsMovies.Tables["Cast"].NewRow();
                    row["MovieId"] = (object)movie.MovieId;
                    row["ActorName"] = (object)movie.Cast[index].Trim();
                    MovieDataSource._dsMovies.Tables["Cast"].Rows.Add(row);
                }
            }
        }

        private MovieData GetMovieData(DataRow r)
        {
            MovieData movieData = new MovieData();
            movieData.MovieId = Convert.ToInt32(r["Id"]);
            movieData.Title = r["Title"].ToString();
            movieData.ReleaseDate = Convert.ToInt32(r["ReleaseDate"]);
            movieData.Rating = Convert.ToInt32(r["Rating"]);
            movieData.Genre = r["Genre"].ToString();
            movieData.Classification = r["Classification"].ToString();
            DataRow[] dataRowArray = MovieDataSource.Movies.Tables["Cast"].Select("MovieId = " + movieData.MovieId.ToString());
            movieData.Cast = new string[dataRowArray.Length];
            for (int index = 0; index < dataRowArray.Length; ++index)
                movieData.Cast[index] = dataRowArray[index]["ActorName"].ToString();
            return movieData;
        }
    }
}
